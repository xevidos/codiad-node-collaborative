collaboration = {
	
	address: null,
	app: null,
	config: null,
	collaborations: [],
	fs: null,
	https: null,
	io: null,
	interval: null,
	options: [],
	port: null,
	projects: {},
	verbose: false,
	
	initialize: function() {
		
		console.log( 'Starting Server' );
		this.fs = require( 'fs' );
		this.config = JSON.parse( this.fs.readFileSync( './config.json' ) );
		this.https = require( 'https' );
		this.address = this.config["address"];
		this.port = this.config["port"];
		this.options = {
			key:    collaboration.fs.readFileSync( collaboration.config[`ssl`][`key`] ),
			cert:   collaboration.fs.readFileSync( collaboration.config[`ssl`][`certificate`] ),
			ca:     collaboration.fs.readFileSync( collaboration.config[`ssl`][`certificate_authority`] )
		};
		this.app = this.https.createServer( this.options );
		this.io = require( 'socket.io' ).listen( this.app );
		this.app.listen( this.port, this.address );
		this.verbose = this.config.verbose;
		console.log( "Server started" );
		
		try {
			
			this.load_intervals();
			this.load_socket();
		} catch( e ) {
			
			this.error( e );
		}
	},
	
	clean_socket: function() {
		
		let _this = collaboration;
		let users = _this.io.sockets.connected;
		let total_users = users.length;
		let time = Date.now();
		
		if( total_users === 0 ) {
			
			//Reset our objects to clean up some memory
			_this.collaborations = [];
			_this.projects = {};
		} else {
			
			/**
			 * Loop through the objects we have to check for users who are
			 * no longer connected, and files that should have been removed.
			 */
			
			try {
				
				let collaboration_keys = Object.keys( _this.collaborations );
				for( let i = collaboration_keys.length; i--; ) {
					
					/*_this.collaborations[collaboration_keys[i]].participants = _this.collaborations[collaboration_keys[i]].participants.filter( function( entry ) {
						
						return collaboration.io.sockets.connected.includes( entry );
					});*/
					
					console.log( _this.collaborations[collaboration_keys[i]].participants,_this.io.sockets.clients()  );
				}
			} catch( e ) {
				
				collaboration.error( e );
			}
		}
	},
	
	error: function( error ) {
		
		console.log( "An error occured: ", error );
		
		if( this.config.reporting ) {
			
			let data = JSON.stringify({
				
				method: "common",
				action: "report",
				parameters: [error],
			});
			let options = {
				hostname: 'api.telaaedifex.com',
				port: 443,
				path: '/',
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Content-Length': data.length,
				},
			};
			
			https.request( options, function( response ) {
				
				let data = '';
				
				// A chunk of data has been recieved.
				resp.on( 'data', ( chunk ) => {
					
					data += chunk;
				});
				
				// The whole response has been received. Print out the result.
				resp.on('end', () => {
				});
				
			}).on( "error", function( error ) {
				
				console.log("Error: " + err.message);
			});
			
			req.write( data );
			req.end();
		}
		
		if( collaboration.config.auto_restart ) {
			
			//collaboration.initialize();
		}
	},
	
	is_connected: function( socket_id ) {
		
		let _this = collaboration;
		let users = _this.io.sockets.connected;
		let total_users = users.length;
		let connected = false;
		
		for( let i = total_users;i--; ) {
			
			if( users[i] == socket_id ) {
				
				connected = true;
				break;
			}
		}
		
		return connected;
	},
	
	load_intervals: function() {
		
		/**
		 * Check and clean the connected users and files every 10 minutes to
		 * keep the server ram usage low, and to allow the fastest experience
		 * to current and connecting users.
		 */
		
		this.interval = setInterval( function() {
			
			console.log( "Cleaning server." );
			let _this = collaboration;
			_this.clean_socket();
		}, 600000 );
		//}, 60000 );
	},
	
	load_socket: function() {
		
		this.io.sockets.on( 'connection', function( socket ) {
			
			_this = collaboration;
			user_id = socket.id;
			file_id = null;
			
			
			
			if( _this.verbose ) {
				
				console.log( "User has connected with a socket id of " + socket.id );
			}
		
			socket.on( 'change', function( file_id, delta ) {
				
				/**
				 * For each user that is connected to the file that was changed send the change.
				 */
				if( _this.verbose ) {
					
					console.log( "change", file_id, delta );
				}
				
				try {
				
					if( file_id !== null && file_id in _this.collaborations ) {
						
						_this.collaborations[file_id]['cached_instructions'].push( delta );
						
						for( var i = _this.collaborations[file_id]['participants'].length; i--; ) {
							
							if( _this.io.sockets.connected[_this.collaborations[file_id]['participants'][i]] !== undefined && socket.id != _this.collaborations[file_id]['participants'][i] ) {
								
								_this.io.sockets.connected[_this.collaborations[file_id]['participants'][i]].emit( "change", file_id, delta );
							}
						}
					} else {
						
						if( _this.verbose ) {
							
							console.log( "WARNING: could not tie file_id to any collaboration session." );
						}
					}
				} catch( e ) {
					
					collaboration.error( e );
				}
			});
			
			socket.on('close_file', function( file_id ) {
				
				console.log( socket.id, " disconnected from ", file_id );
				let found_and_removed = false;
				
				try {
					
					if( file_id in _this.collaborations ) {
						
						let index = _this.collaborations[file_id]['participants'].indexOf( socket.id );
						if( index > -1 ) {
							
							_this.collaborations[file_id]['participants'].splice( index, 1 ) ;
							found_and_removed = true ;
							
							if( _this.collaborations[file_id]['participants'].length === 0 ) {
								
								if( _this.verbose ) {
									
									console.log( "last participant in collaboration, committing to disk & removing from memory" );
								}
								delete _this.collaborations[file_id];
							}
						}
					}
					
					if( ! found_and_removed ) {
						console.log( "WARNING: could not tie socket_id to any collaboration" ) ;
					}
				} catch( e ) {
					
					collaboration.error( e );
				}
				
				file_id = null;
				console.log( _this.collaborations );
			});
			
			socket.on('disconnect', function() {
				
				collaboration.clean_socket();
			});
			
			socket.on( "get_changes", function( file, callback ) {
				
				if( _this.collaborations[file] !== undefined && _this.collaborations[file]['cached_instructions'] !== undefined && ! _this.collaborations[file]['cached_instructions'] == [] ) {
					
					callback( _this.collaborations[file]['cached_instructions'] );
				} else {
					
					callback( {} );
				}
			});
			
			socket.on( "open_file", function( file, delta ) {
				
				let exempt = null;
				
				if( ! ( file in _this.collaborations ) ) {
					
					if( _this.verbose ) {
						
						console.log( "MESSAGE: No file currently in collaboration object creating it." );
					}
					
					_this.collaborations[file] = {'cached_instructions':[], 'participants':[]};
				} else {}
				
				//Add the current user to the participants of the open file.
				_this.collaborations[file]['participants'].push( socket.id );
				
				function recieve_content() {
					
					let content_check = setTimeout( function() {
						
						try {
							
							console.log( 'checking content' );
							
							if( _this.collaborations[file]['cached_instructions'] !== undefined && ! _this.collaborations[file]['cached_instructions'] == [] ) {
								
								//Send the joining user content 
								socket.emit( "load_content", file, _this.collaborations[file]['cached_instructions'], function( delta ) {
									
									console.log( "User's last change" );
									console.log( delta );
									if( ! ( _this.collaborations[file].cached_instructions.length > 0 ) && typeof delta === 'object' && ! ( delta == null ) ) {
										
										//We should always store this change at the begining of the array just in case the user somehow sends a change before this one reaches the server.
										_this.collaborations[file].cached_instructions.unshift( delta );
									}
								});
								clearTimeout( content_check );
								return;
							}
						} catch( e ) {
							
							collaboration.error( e );
						}
					}, 50);
				}
				recieve_content();
			});
			
			socket.on( 'refresh_project', function( path ) {
				
				console.log( path, _this.projects[path] );
				
				try {
					
					if( _this.projects[path] !== undefined ) {
						
						for( let i = _this.projects[path].length; i--; ) {
								
							if( _this.io.sockets.connected[_this.projects[path][i]] !== undefined && socket.id != _this.projects[path][i] ) {
								
								_this.io.sockets.connected[_this.projects[path][i]].emit( "refresh_project", path );
							}
						}
					}
				} catch( e ) {
					
					collaboration.error( e );
				}
			});
			
			socket.on( 'send_buffer', function( file_id ) {
				
				try {
					
					_this.io.sockets.connected[socket.id].emit( "recieve_changes", _this.collaborations[file_id]['cached_instructions'], file_id );
				} catch( e ) {
					
					collaboration.error( e );
				}
			});
			
			
			socket.on( 'set_project', function( path ) {
				
				console.log( "A user is connecting to project " + path );
				
				try {
					
					let keys = Object.keys( _this.projects );
					
					for( let i = keys.length;i--; ) {
						
						if( _this.projects[keys[i]].includes( socket.id ) ) {
							
							_this.projects[keys[i]].splice( _this.projects[keys[i]].indexOf( socket.id ), 1 );
						}
						
						if( _this.projects[keys[i]].length == 0 ) {
							
							delete _this.projects[keys[i]];
						}
					}
					
					if( ! ( path in _this.projects ) ) {
						
						_this.projects[path] = [socket.id];
					} else {
						
						if( ! _this.projects[path].includes( socket.id ) ) {
							
							_this.projects[path].push( socket.id );
						}
					}
				} catch( e ) {
					
					collaboration.error( e );
				}
			});
		});
	},
};

collaboration.initialize();